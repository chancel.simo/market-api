import { Component } from "@angular/core";
import { Global } from "../../global-data.service";

@Component({
    selector: 'app-navbar',
    templateUrl: 'navbar.component.html',
})

export class NavbarComponent {
    isSelected: string;
    constructor() {
        Global.isSelected = 'register';
        console.log('selected value :' + Global.isSelected);
    }
}